def simular_semiparabolico(Vx, Yi):
    
    resultados=[]
    g=-9.8
    
    Viy= 0
    
    y=0 
    
    t=0 
    
    while y >= 0:
        y= Yi + Viy + (0.5)* g* t**2
        y=round(y,3)
        x= Vx*t
        x=round(x,3)
        resultados.append ((x,y))
        t=t+0.2
    return resultados 

d=simular_semiparabolico (3,24)

for posicion in d:
    
    print("x:", posicion[0], "y:", posicion[1])
